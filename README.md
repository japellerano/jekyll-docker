# [Jekyll Docker
Container](https://hub.docker.com/repository/docker/japellerano/jekyll-docker)
### Docker Container for Jekyll CMS Builds

## Reference

		sudo docker build -t jekyll-docker:tag-name . # Build Container
		sudo docker tag short-title japellerano/jekyll-docker:tag-name # Tag
Container Build
		sudo docker push japellerano/jekyll-docker:tag-name # Push Tagged Container
Build to Docker Hub

## Resources
- [Alpine Linux](https://www.alpinelinux.org/)  
- [Build a Minimal Docker Container for Ruby Apps](https://blog.codeship.com/build-minimal-docker-container-ruby-apps/)  
- [How to cache bundle install with Docker](https://medium.com/magnetis-backstage/how-to-cache-bundle-install-with-docker-7bed453a5800)  
- [Docker — A Beginner’s guide to Dockerfile with a sample project](https://medium.com/bb-tutorials-and-thoughts/docker-a-beginners-guide-to-dockerfile-with-a-sample-project-6c1ac1f17490)  
