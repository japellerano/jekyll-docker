FROM alpine:latest
MAINTAINER James Pellerano <james@japellerano.com>

# Packages required for Ruby Gem installation
ENV B_PKG curl-dev ruby-dev build-base
ENV R_PKG ruby ruby-io-console ruby-bundler
ENV W_DIR /tmp/

# Install Pkgs
RUN apk update
RUN apk upgrade
RUN apk add bash $B_PKG $R_PKG

# Clean Up APK Cache
RUN rm -rf /var/cache/apk/*

# Install Gemfile
RUN gem -v
RUN gem install bundler jekyll --no-document


